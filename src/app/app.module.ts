import {ErrorHandler, Injectable, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

// Ionic
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {IonicStorageModule} from '@ionic/storage';
import { AdMobFree } from '@ionic-native/admob-free/ngx';

// App
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

// HTTP
import {HttpClientModule, HttpClient} from '@angular/common/http';

// Translations
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

// Angular FireBase
// import {AngularFireModule} from '@angular/fire';
// import {AngularFireAuthModule} from '@angular/fire/auth';
// import {AngularFireDatabaseModule} from '@angular/fire/database';
// import {AngularFireStorageModule} from '@angular/fire/storage';

// Env
import {environment} from '../environments/environment';

// SQL
import {SQLite} from '@ionic-native/sqlite/ngx';
import {SQLitePorter} from '@ionic-native/sqlite-porter/ngx';

// // Sentry
// import * as Sentry from '@sentry/browser';
//
// // Sentry Bug Tracker
// Sentry.init({
//     dsn: 'https://16ea3b2bbec3405c94c1953576d362f7@sentry.io/1510323'
// });
//
// @Injectable()
// export class SentryErrorHandler implements ErrorHandler {
//     constructor() {
//     }
//
//     handleError(error) {
//         const eventId = Sentry.captureException(error.originalError || error);
//         Sentry.showReportDialog({eventId});
//     }
// }

// Translation Loader
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        HttpClientModule,
        IonicStorageModule.forRoot({
            name: '__langs',
            driverOrder: ['indexeddb', 'sqlite', 'websql']
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        // AngularFireModule.initializeApp(environment.firebase),
        // AngularFireAuthModule,
        // AngularFireDatabaseModule,
        // AngularFireStorageModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        SQLite,
        SQLitePorter,
        AdMobFree,
        // {
        //     provide: ErrorHandler,
        //     useClass: SentryErrorHandler
        // },
        {
            provide: RouteReuseStrategy,
            useClass: IonicRouteStrategy
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
