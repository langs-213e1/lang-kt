import { Injectable } from '@angular/core';
import {LangModel, Langs} from "../../models/langs.model";
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class LangsService {

  constructor(
      private langs: Langs
  ) { }


  getLangModel(value: string): LangModel {
    const langs: LangModel[] = _.uniqWith([].concat(this.langs.interface, this.langs.toLangs, this.langs.fromLangs), _.isEqual);
    return langs.filter(lang => lang.value === value)[0];
  }

  populateSelector(data: any) {
    Object.keys(data).forEach(key => {
      if (data[key] !== null) {
        this.langs.langSelector[key] = this.getLangModel(data[key]);
      }
    });
  }
}
