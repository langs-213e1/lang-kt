import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) {
  }

  private getHeaders(authToken: string, moreHeaders?): HttpHeaders {
    let headers: HttpHeaders = new HttpHeaders();

    if (moreHeaders) {
      Object.keys(moreHeaders).forEach(key => {
        headers = headers.append(key, moreHeaders[key]);
      });
    }

    if (authToken && typeof authToken === 'string') {
      headers = headers.set('Authorization', `${authToken}`);
    }

    return headers;
  }

  get(url: string, params: HttpParams = new HttpParams(), authToken: string = null, moreHeaders: object = {}): Observable<any> {
    const headers = this.getHeaders(authToken, moreHeaders);
    return this.httpClient.get(url, {params, headers});
  }

  delete(url: string, authToken: string = null, moreHeaders: object = {}): Observable<any> {
    const headers = this.getHeaders(authToken, moreHeaders);
    return this.httpClient.delete(url, {headers});
  }

  post(url: string, data: object = {}, authToken: string = null, moreHeaders: object = {}): Observable<any> {
    const headers = this.getHeaders(authToken, moreHeaders);
    return this.httpClient.post(url, data, {headers});
  }

  put(url: string, data: object = {}, authToken: string = null): Observable<any> {
    const headers = this.getHeaders(authToken);
    return this.httpClient.put(url, data, {headers});
  }
}
