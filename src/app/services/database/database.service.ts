import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {SQLiteObject} from '@ionic-native/sqlite';
import {UtilsService} from '../utils/utils.service';
import {StorageService} from '../storage/storage.service';
import {SQLite} from '@ionic-native/sqlite/ngx';
import {SQLitePorter} from '@ionic-native/sqlite-porter/ngx';
import {HttpClient} from '@angular/common/http';
import {Platform} from '@ionic/angular';
import {Translation} from '../../models/translations.model';
import {QueryData} from '../../models/database.model';
import {LangSelector} from '../../models/langs.model';
import * as _ from 'lodash';
import {AdMobService} from '../ad-mob/ad-mob.service';

// tslint:disable-next-line:class-name
interface queryPrepareDataObj {
    data: any[];
    keys: any[];
    set?: string;
}

@Injectable({
    providedIn: 'root'
})
export class DatabaseService {

    private _DB_NAME = '__translations1.db';
    private _SQL_URL = 'assets/sqlite/translations.sql';
    private _DB_STATE: BehaviorSubject<boolean> = new BehaviorSubject(false);
    private _DB: SQLiteObject;

    public error: string;

    constructor(private storageService: StorageService,
                private sqLite: SQLite,
                private sqPorter: SQLitePorter,
                private http: HttpClient,
                private platform: Platform,
                private ads: AdMobService,
                private utilsService: UtilsService) {
    }

    getInit(): Promise<string> {
        const ready = new Promise<string>((resolve) => resolve('done'));

        if (this.platform.is('cordova')) {
                return this.sqLite.create({
                    name: this._DB_NAME,
                    location: 'default'
                }).then(async (db: SQLiteObject) => {
                    this._DB = db;
                    const isData = await this.storageService.getItem('db_translations');

                    if (isData !== 'present') {
                        return this.loadDatabaseData();
                    } else {
                        return ready;
                    }

                }).catch(e => this.error = e);

        } else {
            return ready;
        }

    }

    get databaseState(): Observable<boolean> {
        return this._DB_STATE.asObservable();
    }

    private async loadDatabaseData(): Promise<string> {
         const data = await this.http.get(this._SQL_URL, {responseType: 'text'}).toPromise()

         await this.sqPorter.importSqlToDb(this._DB, data)
            .then(() => {
                this._DB_STATE.next(true);
                this.storageService.setItem('db_translations', 'present');
                return 'done';
            })
            .catch(e => {
                this.error = e;
                this.utilsService.presentToast('db_loading fail');
                return 'fails';
            });

         return new Promise<string>((resolve) => resolve('done'));
    }

    // Queries builders
    private queryPrepareData(record: any): queryPrepareDataObj {
        const data = [];
        const keys = [];
        let set = '';

        Object.keys(record).forEach(key => {
            if (!( _.includes(['open', '_id'], key))) {
                keys.push(`${key}`);

                switch (typeof record[key]) {
                    case 'object':
                        data.push(JSON.stringify(record[key]));
                        set = set + `${key} = ?, `;
                        break;
                    case 'boolean':
                        data.push(record[key] ? 1 : 0);
                        set = set + `${key} = ?, `;
                        break;
                    default:
                        set = set + `${key} = ? `;
                        data.push(record[key]);
                }
            }

        });

        return {
            data,
            keys
        };
    }

    private updateQueryBuilder(table: string, record: any): QueryData {
        const qData: queryPrepareDataObj = this.queryPrepareData(record);

        qData.data.push(record._id);
        return {
            query: `UPDATE ${table} SET ${ qData.keys.join(' = ?, ') } = ? WHERE _id = ?`,
            data: qData.data
        };
    }

    private insertQueryBuilder(table: string, record: any): QueryData {
        const qData: queryPrepareDataObj = this.queryPrepareData(record);
        qData.data.push(record._id);
        qData.keys.push('_id');

        const val = [];
        qData.keys.forEach(() => val.push('?'));

        return {
            query: `INSERT INTO ${table} ( ${qData.keys.join(', ')} ) VALUES ( ${val.join(', ')} );`,
            data: qData.data
        };
    }

    private deleteQueryBuilder(table: string, record: any): QueryData {

        return {
            query: `DELETE FROM ${table} WHERE _id = ?;`,
            data: [record._id]
        };
    }

    // Queries executors
    public sendQuery(query: string): Promise<any> {
        return this._DB.executeSql(query);
    }

    public updateTranslation(langSelector: LangSelector, translation: Translation): Promise<any> {
        const table = langSelector.from.value + '_' + langSelector.to.value;
        const preparedData: QueryData = this.updateQueryBuilder(table, translation);

        this.ads.interstitialAd();

        return this._DB.executeSql(preparedData.query, preparedData.data);
    }

    public addNewTranslation(langSelector: LangSelector, translation: Translation): Promise<any> {
        const table = langSelector.from.value + '_' + langSelector.to.value;
        const preparedData: QueryData = this.insertQueryBuilder(table, translation);

        this.ads.interstitialAd();

        return this._DB.executeSql(preparedData.query, preparedData.data);
    }

    public deleteTranslation(langSelector: LangSelector, translation: Translation): Promise<any> {
        const table = langSelector.from.value + '_' + langSelector.to.value;
        const preparedData: QueryData = this.deleteQueryBuilder(table, translation);

        this.ads.interstitialAd();

        return this._DB.executeSql(preparedData.query, preparedData.data);
    }

    public getTranslations(langSelector: LangSelector,
                           field: string,
                           value: string,
                           limit?: number,
                           offset?: number): Promise<Translation[]> {

        const table = `${langSelector.from.value}_${langSelector.to.value}`;
        const query = `
            SELECT  *
            FROM   ${table}
            WHERE  ${field}
            LIKE  '${value.toLocaleUpperCase()}%'
            ${limit ? `LIMIT ${limit}` : ''}
            ${offset ? `OFFSET ${offset}` : ''}
            `;

        return this._DB.executeSql(query, []).then(res => {

            const tr: Translation[] = [];

            if (res.rows.length > 0) {
                for (let i = 0; i < res.rows.length; i++) {
                    tr.push(new Translation({
                        name: res.rows.item(i).name,
                        defaultName: res.rows.item(i).name,
                        _id: res.rows.item(i)._id,
                        edited: res.rows.item(i).edited,
                        visible: res.rows.item(i).visible,
                        favorite: res.rows.item(i).favorite,
                        groups: JSON.parse(res.rows.item(i).groups),
                        options: JSON.parse(res.rows.item(i).options)
                    } as Translation));
                }
            }

            return tr;
        });
    }

}
