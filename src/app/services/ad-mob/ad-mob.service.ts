import {Injectable} from "@angular/core";
import {
    AdMobFree,
    AdMobFreeBannerConfig,
    AdMobFreeInterstitialConfig
} from '@ionic-native/admob-free/ngx';
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class AdMobService {

    isTesting = false;

    interstitialConfig: AdMobFreeInterstitialConfig = {
        isTesting: this.isTesting,
        autoShow: false,
        id: environment.interstitialAdsId
    };

    constructor(private admobFree: AdMobFree) {

        this.admobFree.on(this.admobFree.events.INTERSTITIAL_LOAD_FAIL).subscribe(res => {
            console.log(res);
        });
    }

    bannerAd() {
        const bannerConfig: AdMobFreeBannerConfig = {
            isTesting: this.isTesting,
            autoShow: true,
            id: environment.bannerAdsId,
            overlap: true,
        };

        this.admobFree.banner.config(bannerConfig);

        return this.admobFree.banner.prepare();
    }

    interstitialPrepare() {
        this.admobFree.interstitial.config(this.interstitialConfig);

        return this.admobFree.interstitial.prepare();
    }

    interstitialAd() {

        this.admobFree.interstitial.isReady()
            .then(res => {
                console.log('res', res);
                this.admobFree.interstitial.show()
                    .then(r => {
                        console.log('r', r)
                    }).catch(e => {
                    console.log('e', e)
                });

            }).catch(e => {
            console.log('er', e)
        });
    }
}

