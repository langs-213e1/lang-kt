import {Injectable} from '@angular/core';
import * as uuid from 'uuid';
import {LangSelector} from "../../models/langs.model";
import {ToastController} from "@ionic/angular";

@Injectable({
    providedIn: 'root'
})
export class UtilsService {

    constructor(
        private toastController: ToastController
    ) {
    }

    capitalize(text) {
        return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
    }

    generateId() {
        return uuid();
    }


    toArray(obj): any[] {
        const tempArray = [];

        // tslint:disable-next-line:forin
        for (const key in Object(obj)) {
            tempArray.push(obj[key]);
        }
        return tempArray;
    }

    getStorageTranslationID(lang: LangSelector): string {
        return `${lang.from.value}-${lang.to.value}`;
    }

    async presentToast(msg, time = 3000) {
        const toast = await this.toastController.create({
            message: msg,
            duration: time
        });
        toast.present();
    }
}
