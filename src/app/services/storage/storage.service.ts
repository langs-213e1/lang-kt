import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor(
        public storage: Storage,
    ) {
    }

    async setItem(key: string, value: any): Promise<any> {
        return await this.storage.set(key, value);
    }

    async getItem(key: string): Promise<any> {
        return await this.storage.get(key);
    }

}
