import {Injectable} from '@angular/core';
import {SettingOption, SettingsOption} from "../../models/settings.model";
import {StorageService} from "../storage/storage.service";
import {LangModel, Langs} from "../../models/langs.model";

@Injectable({
    providedIn: 'root'
})
export class SettingsService {


    public settingModels: SettingsOption[] = [];
    private settings = {};
    constructor(
        private storage: StorageService,
        private langs: Langs
    ) {

        const darkMode: SettingsOption = {
            name: 'THEME.DARK_MODE',
            settingType: 'dark',
            type: 'boolean',
            value: false,
            options: []
        };

        const interfaceLangSetting: SettingsOption = {
            name: 'LANGS.LANG',
            settingType: 'lang',
            type: 'select',
            options: []
        };

        const fromLangSetting: SettingsOption = {
            name: 'LANGS.DEFAULT_FROM',
            settingType: 'default_from',
            type: 'select',
            options: []
        };

        const toLangSetting: SettingsOption = {
            name: 'LANGS.DEFAULT_TO',
            settingType: 'default_to',
            type: 'select',
            options: []
        };

        this.settingModels.push(darkMode);
        // this.langSettingSetter(this.langs.interface, interfaceLangSetting);
        // this.langSettingSetter(this.langs.translation.from, fromLangSetting);
        // this.langSettingSetter(this.langs.translation.to, toLangSetting);
    }

    langSettingSetter(langs: LangModel[], settings: SettingsOption) {
        langs.forEach(lang => {
            settings.options.push(
                {
                    name: lang.name,
                    value: lang.value
                }
            );
        });

        this.settingModels.push(settings);
    }

    get allSettings(): any {
        return this.settings;
    }

    get initSettings(): Promise<string> {
        this.settingModels.forEach(settingModel => {
            this.settings[settingModel.settingType] = null;
        });

        const promises = [];
        const keys = Object.keys(this.settings);
        const _SETTINGS = {};

        keys.forEach(key => {
            promises.push(this.storage.getItem(key));
        });

        return Promise.all(promises).then(
            items => {
                for (let i = 0; i < promises.length; i++) {
                    _SETTINGS[keys[i]] = items[i];
                    switch (keys[i]) {
                        case 'dark':
                            document.body.classList.toggle('dark', items[i]);
                        break;
                    }
                }

                this.settings = _SETTINGS;
                return 'done';
            },
            () => {
                return 'fail';
            });

    }

    getItem(key: string): any {
        return this.settings[key];
    }


    setItem(key, value): Promise<boolean> {
        const _SETTINGS = {...this.settings};

        _SETTINGS[key] = value;

        return this.storage.setItem(key, value).then(
            res => {
                this.settings = _SETTINGS;
                return true;
            },
            error => {
                return false;
            });

    }


    removeItem(key: string): Promise<boolean> {
        const _SETTINGS = {...this.settings};

        _SETTINGS[key] = null;

        return this.storage.setItem(key, null).then(
            () => {
                this.settings = _SETTINGS;
                return true;
            },
            () => {
                return false;
            });
    }

    removeAll(): Promise<boolean> {
        const _SETTINGS = {...this.settings};
        const promises = [];

        Object.keys(_SETTINGS).forEach(key => {
            _SETTINGS[key] = null;
            promises.push(this.storage.setItem(key, null));
        });

        return Promise.all(promises).then(
            () => {
                this.settings = {};
                return true;
            },
            () => {
                return false;
            });
    }
}
