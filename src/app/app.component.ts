import {Component} from '@angular/core';

import {MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {TranslateService} from '@ngx-translate/core';

import {SettingsService} from "./services/settings/settings.service";
import {DatabaseService} from "./services/database/database.service";
import {AdMobService} from "./services/ad-mob/ad-mob.service";


@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

    private promises = [
        this.platform.ready(),
        this.settingsService.initSettings,
        this.database.getInit(),
        this.ads.bannerAd(),
        this.ads.interstitialPrepare(),
    ];

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private translate: TranslateService,
        private settingsService: SettingsService,
        private database: DatabaseService,
        private ads: AdMobService,
        public menuCtrl: MenuController
    ) {

        this.translate.setDefaultLang('ru');

        Promise.all(this.promises)
            .then( (data) => {


                setTimeout(() => {
                    this.ads.interstitialAd();
                }, (30 * 1000));

                console.log('platform.ready', data);

                // const lang = this.settingsService.getItem('lang');
                // if (lang) {
                //     this.translate.use(lang);
                // }

                this.translate.use('ru');

                this.statusBar.styleDefault();
                this.splashScreen.hide();
            })
            .catch(error => {
                console.log(error);
            });
    }

    close() {
        this.menuCtrl.close('main-menu');
    }
}
