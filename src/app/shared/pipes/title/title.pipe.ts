import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'title'
})
export class TitlePipe implements PipeTransform {

  transform(value: any, args?: any): any {
      if (value === null) return '';
      const v = value.toLowerCase();
      return v.charAt(0).toUpperCase() + v.slice(1);
  }

}
