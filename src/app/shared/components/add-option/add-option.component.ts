import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Option} from "../../../models/translations.model";

@Component({
  selector: 'app-add-option',
  templateUrl: './add-option.component.html',
  styleUrls: ['./add-option.component.scss'],
})
export class AddOptionComponent implements OnInit {

  @Output() newOption: EventEmitter<any> = new EventEmitter();
  optionKey = "";
  constructor() { }

  ngOnInit() {

  }

  addOptionHandler() {
    this.newOption.emit({
      option: new Option(this.optionKey),
      update: false
    });

    this.optionKey = ""
  }
}
