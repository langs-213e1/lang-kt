import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Translation} from "../../../models/translations.model";
import {Router} from "@angular/router";
import {ScrollLoader} from "../../../models/database.model";
import {DatabaseService} from "../../../services/database/database.service";
import {AlertController, LoadingController, PopoverController} from "@ionic/angular";
import {TranslateService} from "@ngx-translate/core";
import {Langs} from "../../../models/langs.model";
import {UtilsService} from "../../../services/utils/utils.service";
import {PopoverMenuComponent} from "../popover-menu/popover-menu.component";
import {PopoverModel} from "../../../models/popover.model";

@Component({
    selector: 'app-translation-item-list',
    templateUrl: './translation-item-list.component.html',
    styleUrls: ['./translation-item-list.component.scss'],
})
export class TranslationItemListComponent implements OnInit {

    @Input() translations: Translation[] = [];
    translationsText: any;
    popover: any;

    @Output() loadQuery: EventEmitter<ScrollLoader> = new EventEmitter<ScrollLoader>();

    constructor(private router: Router,
                private loadingController: LoadingController,
                private langs: Langs,
                private popoverController: PopoverController,
                private utilsService: UtilsService,
                private translate: TranslateService,
                private alertController: AlertController,
                private dataBase: DatabaseService) {

        this.translate.get(
            [
                'MODAL.CONFIRM_TITLE',
                'MODAL.DELETE_ACTION',
                'MODAL.CANCEL_BUTTON',
                'MODAL.DELETE_BUTTON',
                'MODAL.DELETING',
                'MODAL.DONE',
                'MESSAGE.ERROR',

            ]
        ).subscribe(translations => this.translationsText = translations);
    }

    ngOnInit() {
    }


    openTranslation(index) {
        this.translations[index].open = !this.translations[index].open;
    }

    async openItem(item) {
        await this.popover.dismiss();

        this.router.navigate(['item-manipulation'], {
            queryParams: {
                type: 'edit',
                item: JSON.stringify(item)
            }
        });
    }

    async presentPopover(ev: any, item: any) {
        const popData: PopoverModel[] = [
            {
                name: 'MODAL.EDIT_BUTTON',
                callback: () => this.openItem(item)
            },
            {
                name: 'MODAL.DELETE_BUTTON',
                callback: () => this.deleteItem(item)
            },
        ];

        this.popover = await this.popoverController.create({
            component: PopoverMenuComponent,
            componentProps: {
                data: popData,
            },
            event: ev,
        });
        return await this.popover.present();
    }

    async deleteItem(item) {

        const alert = await this.alertController.create({
                header: this.translationsText['MODAL.CONFIRM_TITLE'],
                message: this.translationsText['MODAL.DELETE_ACTION'],
                buttons: [
                    {
                        text: this.translationsText['MODAL.CANCEL_BUTTON']
                    },
                    {
                        text: this.translationsText['MODAL.DELETE_BUTTON'],
                        handler: async () => {
                            const loading = await this.loadingController.create({
                                message: this.translationsText['MODAL.DELETING']
                            });
                            await loading.present();

                            this.dataBase.deleteTranslation(this.langs.langSelector, item)
                                .then(res => {
                                    console.log(res);
                                    this.utilsService.presentToast(this.translationsText['MODAL.DONE']);
                                    this.translations = this.translations.filter(e => e._id !== item._id);
                                    loading.dismiss();
                                    this.popover.dismiss();
                                })
                                .catch(error => {
                                    console.log(error);
                                    this.utilsService.presentToast(this.translationsText['MESSAGE.ERROR']);
                                    loading.dismiss();
                                    this.popover.dismiss();
                                });
                            return false;
                        }
                    }
                ]
            });

        await alert.present();
    }

    loadingHandler(event) {
        this.loadQuery.emit(
            {
                offset: this.translations.length,
                loader: event
            }
        );
    }

}
