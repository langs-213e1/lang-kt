import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Option, Translation} from "../../../models/translations.model";
import {AlertController} from "@ionic/angular";

@Component({
  selector: 'app-option-list',
  templateUrl: './option-list.component.html',
  styleUrls: ['./option-list.component.scss'],
})
export class OptionListComponent implements OnInit {

  @Input() item: Translation;
  @Input() editable = false;

  @Output() optionUpdate: EventEmitter<any> = new EventEmitter();

  constructor(
      private alertController: AlertController
  ) { }

  ngOnInit() {}

  deleteOptionHandler(i) {
    this.item.options = this.item.options.filter(o => o._id !== i._id);
  }

  async updateOptionHandler(option: Option) {
    const alert = await this.alertController.create({
      header: option.key,
      inputs: [
        {
          name: 'key',
          type: 'text',
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        }, {
          text: 'Ok',
          handler: data => {
            option.key = data.key;
            if (!option.edited) {
              option.defaultValue = data.key
            }

            this.optionUpdate.emit({
              option: option,
              update: true
            })
          }
        }
      ]
    });

    await alert.present();
  }

}
