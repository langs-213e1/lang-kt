import {Component, OnInit} from '@angular/core';
import {Langs} from "../../../models/langs.model";
import {SettingsService} from "../../../services/settings/settings.service";
import {LangsService} from "../../../services/langs/langs.service";

@Component({
    selector: 'app-lang-selector',
    templateUrl: './lang-selector.component.html',
    styleUrls: ['./lang-selector.component.scss'],
})
export class LangSelectorComponent implements OnInit {

    langSelector = {
        from: 'ru',
        to: 'tr',
    };
    langs: Langs;

    constructor(
        private langsOptions: Langs,
        private langsService: LangsService,
    ) {
        this.langs = langsOptions;
    }

    ngOnInit() {
        this.langsService.populateSelector(this.langSelector);
    }

    swapHandler() {
        const temp = this.langSelector.to;

        this.langSelector.to = this.langSelector.from;
        this.langSelector.from = temp;

    }

    modelChangeHandler(type, data) {
        this.langSelector[type] = data.detail.value;

        if (this.langSelector.from === this.langSelector.to) {
            this.langSelector[type] = null;
        }

        this.langsService.populateSelector(this.langSelector);
    }

}
