import {Component, OnInit} from '@angular/core';
import {PopoverModel} from "../../../models/popover.model";

@Component({
    selector: 'app-popover-menu',
    template: `
        <ion-list>
            <ion-item
                    *ngFor="let menu of popoverData"
                    (click)="menu.callback()"
                    button>
                {{menu.name | translate}}
            </ion-item>
        </ion-list>
    `,
    styleUrls: [],
})
export class PopoverMenuComponent implements OnInit {

    popoverData: PopoverModel[] = [];

    constructor() {

    }

    ngOnInit() {
        const component: any = this;

        this.popoverData = component.data;
    }
}
