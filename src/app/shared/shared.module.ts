import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LangSelectorComponent } from './components/lang-selector/lang-selector.component';
import {IonicModule} from "@ionic/angular";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {TranslationItemListComponent} from "./components/translation-item-list/translation-item-list.component";
import {OptionListComponent} from "./components/option-list/option-list.component";
import {AddOptionComponent} from "./components/add-option/add-option.component";
import {PopoverMenuComponent} from "./components/popover-menu/popover-menu.component";
import { TitlePipe } from './pipes/title/title.pipe';

@NgModule({
    declarations: [
        LangSelectorComponent,
        TranslationItemListComponent,
        OptionListComponent,
        AddOptionComponent,
        PopoverMenuComponent,
        TitlePipe
    ],
    imports: [
        CommonModule,
        IonicModule,
        TranslateModule,
        FormsModule
    ],
    exports: [
        LangSelectorComponent,
        TranslationItemListComponent,
        OptionListComponent,
        AddOptionComponent,
        PopoverMenuComponent,
        TitlePipe
    ],
    entryComponents: [
        PopoverMenuComponent
    ]
})
export class SharedModule { }
