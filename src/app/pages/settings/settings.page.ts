import {Component, OnInit} from '@angular/core';
import {SettingsOption} from "../../models/settings.model";
import {SettingsService} from "../../services/settings/settings.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

    settingsForm: SettingsOption[] = this.settingsService.settingModels;
    settingsItem = this.settingsService.allSettings;

    constructor(
        private settingsService: SettingsService,
        private translate: TranslateService,
    ) {
        this.settingsForm.forEach(elm => {
            if ( elm.value !== undefined) {
                elm.value = this.settingsItem[elm.settingType];
            }
        });
    }

    ngOnInit() {}

    settingsChangeHandler(type: string, data: any) {
        switch (type) {
            case 'lang':
                this.translate.use(data);
                break;
            case 'dark':
                document.body.classList.toggle('dark', data);
                break;
            default:
                break;
        }
        this.settingsService.setItem(type, data);

    }

}
