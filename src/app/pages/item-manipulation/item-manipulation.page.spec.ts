import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemManipulationPage } from './item-manipulation.page';

describe('ItemManipulationPage', () => {
  let component: ItemManipulationPage;
  let fixture: ComponentFixture<ItemManipulationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemManipulationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemManipulationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
