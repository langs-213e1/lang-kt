import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Option, Translation} from '../../models/translations.model';
import {BehaviorSubject, Subscription} from 'rxjs';
import {UtilsService} from '../../services/utils/utils.service';
import {DatabaseService} from '../../services/database/database.service';
import {Langs} from '../../models/langs.model';
import {AlertController, LoadingController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {AdMobService} from "../../services/ad-mob/ad-mob.service";


@Component({
    selector: 'app-item-manipulation',
    templateUrl: './item-manipulation.page.html',
    styleUrls: ['./item-manipulation.page.scss'],
})
export class ItemManipulationPage implements OnInit, OnDestroy {

    item: Translation;
    type: string;
    translationsText: any;
    needSave: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    routerSubscription: Subscription;

    langs: Langs;

    constructor(private route: ActivatedRoute,
                private utilsService: UtilsService,
                private ads: AdMobService,
                private database: DatabaseService,
                private langsOptions: Langs,
                private alertController: AlertController,
                private translate: TranslateService,
                private loadingController: LoadingController,
                private router: Router) {

        this.langs = langsOptions;
    }

    ngOnInit() {
        this.translate.get(
            [
                'MODAL.CONFIRM_TITLE',
                'MODAL.SAVE_ACTION',
                'MODAL.CANCEL_BUTTON',
                'MODAL.SAVE_BUTTON',
                'MODAL.SAVING',
                'MODAL.SAVED',
                'MODAL.DONE',
                'MESSAGE.ERROR',

            ]
        ).subscribe(translations => this.translationsText = translations);

        this.routerSubscription = this.route.queryParams.subscribe((params: any) => {

            if (params && params.item && params.type) {
                this.type = params.type;
                this.item = JSON.parse(params.item);

                if (this.type === 'edit') {
                    this.needSave.next(true);
                }
            } else {
                const newOptions: Option[] = [];
                this.item =
                    new Translation({
                        name: '',
                        defaultName: '',
                        edited: true,
                        _id: this.utilsService.generateId(),
                        visible: true,
                        favorite: false,
                        groups: [],
                        options: newOptions
                    })
                ;
            }

        });
    }

    ngOnDestroy() {
        this.routerSubscription.unsubscribe();
    }

    async saveTranslation() {
        const alert = await this.alertController.create({
            header: this.translationsText['MODAL.CONFIRM_TITLE'],
            message: this.translationsText['MODAL.SAVE_ACTION'],
            buttons: [
                {
                    text: this.translationsText['MODAL.CANCEL_BUTTON']
                },
                {
                    text: this.translationsText['MODAL.SAVE_BUTTON'],
                    handler: async () => {
                        const loading = await this.loadingController.create({
                            message: this.translationsText['MODAL.SAVING']
                        });
                        await loading.present();

                        let query: Promise<any>;

                        if (this.type === 'edit') {
                            query = this.database.updateTranslation(this.langs.langSelector, this.item);
                        } else {
                            query = this.database.addNewTranslation(this.langs.langSelector, this.item);
                        }

                        await this.ads.interstitialPrepare();

                        this.ads.interstitialAd();

                        query
                            .then(res => {
                                console.log(res);
                                this.utilsService.presentToast(this.translationsText['MODAL.DONE']);
                                this.needSave.next(false);
                                loading.dismiss();
                                this.goBack(this.item);
                            })
                            .catch(error => {
                                console.log(error);
                                this.utilsService.presentToast(this.translationsText['MESSAGE.ERROR']);
                                this.needSave.next(false);
                                loading.dismiss();
                            });
                        return false;
                    }
                }
            ]
        });

        if (this.item.name && this.item.options.length > 0) {
            await alert.present();
        }
    }

    goBack(item) {
        this.router.navigate(['search'], {
            queryParams: {
                item: JSON.stringify(item)
            }
        });
    }

    optionsManagerHandler(data: any) {
        this.needSave.next(true);
        if (data.update) {
            this.item.options = this.item.options.filter(e => e._id !== data.option._id);
            this.item.options.push(data.option);
        } else {
            this.item.options.push(data.option);
        }
    }

    titleChangeHandler(text) {
        this.item.name = text.detail.value;
        if (this.type !== 'edit') {
            this.item.defaultName = text.detail.value;
        }
        this.needSave.next(true);
    }
}
