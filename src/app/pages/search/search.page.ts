import {Component, OnInit} from '@angular/core';
import {Option, Translation, createTranslation} from '../../models/translations.model';
import {ActivatedRoute, Router} from "@angular/router";
import {UtilsService} from "../../services/utils/utils.service";
import {DatabaseService} from "../../services/database/database.service";
import {Langs} from "../../models/langs.model";
import {Platform} from "@ionic/angular";
import {ScrollLoader} from "../../models/database.model";
import {Subscription} from "rxjs/index";
@Component({
    selector: 'app-search',
    templateUrl: './search.page.html',
    styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

    translations: Translation[] = [];
    searchText: string;
    routerSubscription: Subscription;

    constructor(
        private router: Router,
        private utilsService: UtilsService,
        private database: DatabaseService,
        private langs: Langs,
        private platform: Platform,
        private route: ActivatedRoute
    ) {
        this.routerSubscription = this.route.queryParams.subscribe((params: any) => {

            if (params && params.item) {
                const newItem = createTranslation(JSON.parse(params.item));

                this.translations = this.translations.map(t => t._id === newItem._id ? newItem : t );
            }
        });
    }

    ngOnInit() {
        this.translations = [];
    }

    searchInputHandler(event) {
        this.searchText = event;
        if (this.searchText === '') {
            this.translations = [];
        } else {
            if (this.platform.is('cordova')) {
                this.database.getTranslations(this.langs.langSelector, 'name', this.searchText, 20)
                    .then(res => {
                        this.translations = res;
                    })
                    .catch(error => {
                        this.utilsService.presentToast('search fails');
                    });
            } else {
                for (let i = 0; i < 20; i++) {
                    const newOptions: Option[] = [new Option('orem oremoremoremoremorem orem orem orem orem orem orem orem orem orem orem orem orem orem test testtesttesttesttest testtesttest testtest'),
                        new Option('orem oremoremoremoremorem orem orem orem orem orem orem orem orem orem orem orem orem orem test testtesttesttesttest testtesttest testtest')];
                    this.translations.push(
                        new Translation({
                            name: `${i} ${this.utilsService.generateId()}`,
                            defaultName: '',
                            edited: true,
                            _id: this.utilsService.generateId(),
                            visible: true,
                            favorite: false,
                            groups: [],
                            options: newOptions
                        })
                    );
                }
            }
        }
    }

    getMoreRecordsHandler(scrollLoader: ScrollLoader) {
        if (this.searchText === '') {
            this.translations = [];
            scrollLoader.loader.target.complete();
        } else {
            if (this.platform.is('cordova')) {
                this.database.getTranslations(
                    this.langs.langSelector,
                    'name',
                    this.searchText,
                    20,
                    scrollLoader.offset)
                    .then(res => {
                        this.translations = this.translations.concat(res);
                        scrollLoader.loader.target.complete();
                    })
                    .catch(() => {
                        this.utilsService.presentToast('search fails');
                        scrollLoader.loader.target.complete();
                    }).finally(() => {
                    scrollLoader.loader.target.complete()
                });

            } else {
                setTimeout(() => {
                    for (let i = 0; i < 20; i++) {
                        const newOptions: Option[] = [ new Option('ok') ];
                        this.translations.push(
                            new Translation({
                                name: `${i} ${this.utilsService.generateId()}`,
                                defaultName: '',
                                edited: true,
                                _id: this.utilsService.generateId(),
                                visible: true,
                                favorite: false,
                                groups: [],
                                options: newOptions
                            })
                        );
                    }
                    scrollLoader.loader.target.complete();
                }, 1000);
            }
        }
    }

    newTranslationHandler() {
        this.router.navigate(['item-manipulation'], {
            queryParams: {
                type: 'new',
                item: null
            }
        });
    }

}
