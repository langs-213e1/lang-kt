import {Injectable} from '@angular/core';

export interface LangsModel {
    from: LangModel[];
    to: LangModel[];
}

export interface LangModel {
    name: string;
    value: string;
}

export interface LangSelector {
    from: LangModel;
    to: LangModel;
}


@Injectable({
    providedIn: 'root'
})
export class Langs {
    public fromLangs: LangModel[] = [
        {
            name: 'LANGS.RU',
            value: 'ru'
        },
        {
            name: 'LANGS.TR',
            value: 'tr'
        }
    ];

    public toLangs: LangModel[] = [
        {
            name: 'LANGS.RU',
            value: 'ru'
        },
        {
            name: 'LANGS.TR',
            value: 'tr'
        }
    ];

    public translation: LangsModel = {
        from: this.fromLangs,
        to: this.toLangs
    };

    public interface: LangModel[] = [
        {
            name: 'LANGS.RU',
            value: 'ru'
        },
        {
            name: 'LANGS.EN',
            value: 'en'
        },
        {
            name: 'LANGS.UA',
            value: 'ua'
        }
    ];

    public langSelector: LangSelector = {
        from: {
            name: 'LANGS.RU',
            value: 'ru'
        },
        to: {
            name: 'LANGS.TR',
            value: 'tr'
        }
    };
}
