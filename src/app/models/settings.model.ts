export interface SettingsOption {
    name: string;
    type: string;
    settingType: string;
    value?: any;
    options: SettingOption[];
}

export interface SettingOption {
    name: string;
    value: string;
}
