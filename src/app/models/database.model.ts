export interface QueryData {
    query: string;
    data: any[];
}
export interface ScrollLoader {
    offset: number;
    loader: any;
}
