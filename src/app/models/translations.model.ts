import * as uuid from 'uuid';

export const createTranslation = (item: any): Translation => {
    const options: Option[] = [];

    item.options.forEach(o => {
        const newOpt: Option = new Option(o.key);
        newOpt.update(o);

        options.push(newOpt);
    });

    item.options = options;

    return new Translation(item);
};



export class Translation {
    name: string;
    defaultName: string;
    edited: boolean;
    _id: string;
    visible: boolean;
    favorite: boolean;
    groups: string[];
    open?: boolean;
    options: Option[];

    constructor(translation: Translation) {
        Object.assign(this, translation);
    }
}

export class Option {
    key: string;
    edited: boolean;
    visible: boolean;
    defaultValue: string;
    _id: string;

    constructor(key?: string) {
        this.key = key;
        this.edited = false;
        this.visible = true;
        this.defaultValue = key;
        this._id = uuid();
    }

    update(values: UpdateOption) {

        Object.keys(values).forEach(key => {
            this[key] = values[key];
        });

        if (this.defaultValue === undefined) {
            this.defaultValue = values.key;
        }
    }
}
interface UpdateOption {
    key?: string;
    edited?: boolean;
    visible?: boolean;
}
