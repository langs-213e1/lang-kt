// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyD-YIpQ38U9jrsZejGm6hnSzzAQG9QEeyA",
        authDomain: "lang-38fe2.firebaseapp.com",
        databaseURL: "https://lang-38fe2.firebaseio.com",
        projectId: "lang-38fe2",
        storageBucket: "",
        messagingSenderId: "789778272639",
        appId: "1:789778272639:web:db94c4fe86a7d3fc"
    },
    bannerAdsId: "ca-app-pub-1443599759898001/6146202844",
    interstitialAdsId: "ca-app-pub-1443599759898001/4522229014"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
