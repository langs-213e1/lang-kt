CREATE TABLE IF NOT EXISTS `ru_tr` (
	`name`	        TEXT NOT NULL,
	`defaultName`	TEXT NOT NULL,
	`edited`	    INTEGER NOT NULL,
	`visible`	    INTEGER NOT NULL,
	`favorite`	    INTEGER NOT NULL,
	`groups`	    TEXT NOT NULL,
	`options`	    INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS `tr_ru` (
	`name`	        TEXT NOT NULL,
	`defaultName`	TEXT NOT NULL,
	`edited`	    INTEGER NOT NULL,
	`visible`	    INTEGER NOT NULL,
	`favorite`	    INTEGER NOT NULL,
	`groups`	    TEXT NOT NULL,
	`options`	    INTEGER NOT NULL
);
